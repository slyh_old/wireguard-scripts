#!/bin/bash

RELAY_PRIVATE_IP=172.26.7.144
WG_HOST_IP=172.26.13.198
WG_HOST_PORT=51820

# Enable IP forwarding
sysctl -w net.ipv4.ip_forward=1

# Flush all chains
/sbin/iptables -F
/sbin/iptables -t nat -F
/sbin/iptables -t mangle -F

# Delete all chains
/sbin/iptables -X

# Default policy
/sbin/iptables -P INPUT DROP
/sbin/iptables -P FORWARD DROP
/sbin/iptables -P OUTPUT ACCEPT

# Allow established sessions and SSH
/sbin/iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
/sbin/iptables -A INPUT -p tcp --dport 22 -j ACCEPT
/sbin/iptables -A INPUT -j DROP

# Forward rules
/sbin/iptables -A FORWARD -d $WG_HOST_IP -i eth0 -o eth0 -j ACCEPT
/sbin/iptables -A FORWARD -s $WG_HOST_IP -i eth0 -o eth0 -j ACCEPT
/sbin/iptables -A FORWARD -j DROP

# NAT to WireGuard server
/sbin/iptables -t nat -A PREROUTING -p udp --dport $WG_HOST_PORT -j DNAT --to-destination $WG_HOST_IP:$WG_HOST_PORT
/sbin/iptables -t nat -A POSTROUTING -p udp -d $WG_HOST_IP --dport $WG_HOST_PORT -j SNAT --to-source $RELAY_PRIVATE_IP