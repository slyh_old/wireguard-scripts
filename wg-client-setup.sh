#!/bin/bash

WG_HOST_ENDPOINT=54.168.76.50:51820
WG_HOST_KEY=X6x2sXs45nb0UGQNrUFyZPW9swQZ3Xh/qFbhGFEmGxI=
WG_IF_ADDR=203.0.113.2/32

# Install WireGuard
apt install wireguard

# Generate key for WireGuard
cd /etc/wireguard
rm *
umask 077
wg genkey > wg.key
wg pubkey < wg.key

# Generate WireGuard config file
touch /etc/wireguard/wg0.conf
ip link add dev wg0 type wireguard
ip address add dev wg0 $WG_IF_ADDR
wg set wg0 private-key /etc/wireguard/wg.key
wg set wg0 peer $WG_HOST_KEY endpoint $WG_HOST_ENDPOINT allowed-ips 0.0.0.0/0
wg-quick save wg0