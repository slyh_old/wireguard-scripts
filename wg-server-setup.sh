#!/bin/bash

# Install WireGuard
apt install wireguard

# Generate key for WireGuard
cd /etc/wireguard
rm *
umask 077

wg genkey > wg0.key
wg pubkey < wg0.key

wg genkey > wg1.key
wg pubkey < wg1.key