#!/bin/bash

# Setup WireGuard interface
ip link add dev wg0 type wireguard
ip address add dev wg0 203.0.113.1/24
wg set wg0 listen-port 51820 private-key /etc/wireguard/wg0.key
wg set wg0 peer iP+bxWn/4ftErjfL29QugxhWmoVh7ByfBEAKMGJM01g= allowed-ips 203.0.113.2/32
ip link set wg0 up

ip link add dev wg1 type wireguard
ip address add dev wg1 10.0.1.1/24
wg set wg1 listen-port 51821 private-key /etc/wireguard/wg1.key
wg set wg1 peer iP+bxWn/4ftErjfL29QugxhWmoVh7ByfBEAKMGJM01g= allowed-ips 10.0.0.0/8
ip link set wg1 up