#!/bin/bash

# Block access from WireGuard clients
/sbin/iptables -A INPUT -i wg0 -j DROP
/sbin/iptables -A INPUT -i wg1 -j DROP

# Allow established sessions
/sbin/iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Allow SSH, WireGuard and local resolver
/sbin/iptables -A INPUT -p tcp --dport 22 -j ACCEPT
/sbin/iptables -A INPUT -p udp --dport 51820 -j ACCEPT
/sbin/iptables -A INPUT -p udp --dport 53 -s 127.0.0.1 -d 127.0.0.53 -i lo -j ACCEPT

# Forward whitelists
/sbin/iptables -A FORWARD -d 172.16.0.0/12 -j DROP

/sbin/iptables -A FORWARD -d 10.0.0.0/8 -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
/sbin/iptables -A FORWARD -d 203.0.113.0/24 -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

/sbin/iptables -A FORWARD -d 203.0.113.0/24 -j DROP

/sbin/iptables -A FORWARD -s 10.0.0.0/8 -i wg1 -o eth0 -j ACCEPT
/sbin/iptables -A FORWARD -s 203.0.113.0/24 -i wg0 -o eth0 -j ACCEPT
/sbin/iptables -A FORWARD -s 203.0.113.0/24 -d 10.0.0.0/8 -i wg0 -o wg1 -j ACCEPT

# NAT Masquerade
/sbin/iptables -t nat -A POSTROUTING -s 10.0.0.0/8 -d !203.0.113.0/24 -o eth0 -j MASQUERADE
/sbin/iptables -t nat -A POSTROUTING -s 203.0.113.0/24 -d !10.0.0.0/8 -o eth0 -j MASQUERADE