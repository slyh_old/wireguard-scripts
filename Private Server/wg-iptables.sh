#!/bin/bash

# Allow established sessions
/sbin/iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Allow SSH, WireGuard and local resolver
/sbin/iptables -A INPUT -p tcp --dport 22 -j ACCEPT
/sbin/iptables -A INPUT -p udp --dport 51820 -j ACCEPT
/sbin/iptables -A INPUT -p udp --dport 53 -s 127.0.0.1 -d 127.0.0.53 -i lo -j ACCEPT

# Forward whitelist
#/sbin/iptables -A FORWARD -d 172.16.0.0/12 -j DROP
/sbin/iptables -A FORWARD -s 203.0.113.0/24 -i wg0 -o eth0 -j ACCEPT
/sbin/iptables -A FORWARD -d 203.0.113.0/24 -i eth0 -o wg0 -j ACCEPT

# NAT Masquerade
/sbin/iptables -t nat -A POSTROUTING -s 203.0.113.0/24 -o eth0 -j MASQUERADE