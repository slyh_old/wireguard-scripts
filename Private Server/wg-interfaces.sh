#!/bin/bash

# Setup Wireguard interface
ip link add dev wg0 type wireguard
ip address add dev wg0 203.0.113.1/24
wg set wg0 listen-port 51820 private-key /etc/wireguard/wg0.key
wg set wg0 peer iP+bxWn/4ftErjfL29QugxhWmoVh7ByfBEAKMGJM01g= allowed-ips 203.0.113.2/32
ip link set wg0 up