#!/bin/bash

DIR=$(dirname "$0")

# Enable IP forwarding
sysctl -w net.ipv4.ip_forward=1

# Flush all chains
/sbin/iptables -F
/sbin/iptables -t nat -F
/sbin/iptables -t mangle -F

# Delete all chains
/sbin/iptables -X

# Default policy
/sbin/iptables -P INPUT DROP
/sbin/iptables -P FORWARD DROP
/sbin/iptables -P OUTPUT ACCEPT

# Custom iptables
$DIR/wg-iptables.sh

# Setup WireGuard interface
$DIR/wg-interfaces.sh