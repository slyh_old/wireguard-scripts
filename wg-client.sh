#!/bin/bash

WIN_SRC_ADDR=198.51.100.0/24
PUBLIC_IF=eth0
PRIVATE_IF=eth1

# Enable IP forwarding
sysctl -w net.ipv4.ip_forward=1

# Flush all chains
/sbin/iptables -F
/sbin/iptables -t nat -F
/sbin/iptables -t mangle -F

# Delete all chains
/sbin/iptables -X

# Default policy
/sbin/iptables -P INPUT DROP
/sbin/iptables -P FORWARD DROP
/sbin/iptables -P OUTPUT ACCEPT

# Allow established sessions and local resolver
/sbin/iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
/sbin/iptables -A INPUT -p udp --dport 53 -s 127.0.0.1 -d 127.0.0.53 -i lo -j ACCEPT
/sbin/iptables -A INPUT -j DROP

# Drop forward request from eth0
/sbin/iptables -A FORWARD -i $PUBLIC_IF -j DROP
/sbin/iptables -A FORWARD -o $PUBLIC_IF -j DROP
/sbin/iptables -A FORWARD -s $WIN_SRC_ADDR -i $PRIVATE_IF -o wg0 -j ACCEPT
/sbin/iptables -A FORWARD -d $WIN_SRC_ADDR -i wg0 -o $PRIVATE_IF -j ACCEPT
/sbin/iptables -A FORWARD -j DROP

# NAT Masquerade
/sbin/iptables -t nat -A POSTROUTING -s $WIN_SRC_ADDR -o wg0 -j MASQUERADE

wg-quick up wg0